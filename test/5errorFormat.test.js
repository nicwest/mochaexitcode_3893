const assert = require('chai').assert
const sinon = require('sinon')

function determineErrorCode(err) {
    let isJoi = false
    err = Array.isArray(err) ? err : [err]
    for (const e of err) {
        if ((e.isJoi) || (e.error && e.error.isJoi)) {
            isJoi = true
        }
    }

    if (isJoi) { return 400 }
    else if (err.code) {
        const intCode = parseInt(err.code, 10)
        if (intCode >= 400 && intCode < 600) {
            return intCode
        }
    }

    return 500
}

function errorFormat(err, req, res, next) {
    const status = determineErrorCode(err)
    res.status(status)
    if (err.stack) err.stack = err.stack.split('\n')

    if (err.length) {
        res.json({
            error: true,
            message: err.map(e => e.message),
            stack: err.map(e => e.stack),
            _object: req.body,
        })
    }
    else {
        res.json({
            error: true,
            message: err.message,
            stack: err.stack,
            _object: req.body,
        })
    }
}

class mockResp {
    constructor() {
        this.statusCode = null
        this.body = null
    }
    status(code) {
        if (code) this.statusCode = code

        return this.statusCode
    }
    json(data) {
        this.body = JSON.stringify(data)
    }
}

describe('boundary', () => {
    it('should use error status code, if ones provided', () => {
        const err = new Error('418-Error')
        err.code = 418
        err.stack = 'fake-stack'
        const req = {
            body: ['test body'],
        }
        const res = new mockResp()

        errorFormat(err, req, res, null)

        assert.equal(res.status(), 418)
    })
    it('should return a 500 if a node-error.code is generated', () => {
        const err = new Error('test')
        err.code = 'ERR_ARG_NOT_ITERABLE'
        err.stack = 'fake-stack'
        const req = { body: ['test body'] }
        const res = new mockResp()
        errorFormat(err, req, res, null)

        assert.equal(res.status(), 500)
    })
    it('should return 500 if a >599 error code is set', () => {
        const err = new Error('test')
        err.code = 618
        err.stack = 'fake-stack'
        const req = { body: ['test body'] }
        const res = new mockResp()
        errorFormat(err, req, res, null)

        assert.equal(res.status(), 500)
    })
    it('should return 500 if a <500 error code is set', () => {
        const err = new Error('test')
        err.code = 215
        err.stack = 'fake-stack'
        const req = { body: ['test body'] }
        const res = new mockResp()
        errorFormat(err, req, res, null)

        assert.equal(res.status(), 500)
    })
    it('should return a 500 if no error info is provided', () => {
        const err = new Error('500-Error')
        err.stack = 'fake-stack'
        const req = {
            body: ['test body'],
        }
        const res = new mockResp()

        errorFormat(err, req, res, null)

        assert.equal(res.status(), 500)
    })
    it('request body is returned in _object, if one was sent', () => {
        const err = new Error('Test-Error')
        err.stack = 'fake-stack'
        const req = {
            body: {
                props: 'random string',
                vals: 'that random val',
            },
        }
        const res = new mockResp()

        errorFormat(err, req, res, null)

        assert.equal(res.statusCode, 500)
        assert.equal(res.body, JSON.stringify({
            error: true,
            message: 'Test-Error',
            stack: ['fake-stack'],
            _object: {
                props: 'random string',
                vals: 'that random val',
            },
        }))
    })
    it('returns json via res.json', () => {
        const err = new Error('Test-Error')
        err.stack = 'fake-stack'
        const req = {
            body: ['test body'],
        }
        const res = new mockResp()
        res.json = sinon.spy()

        errorFormat(err, req, res, null)

        assert.equal(res.status(), 500)
        assert.isTrue(res.json.called)
    })
    it('error stacks are returned as an array', () => {
        const err = new Error('Stack-Error')
        err.stack = 'not like this the-other-thing.js:111\n' +
            'oh god that.js:118\n' +
            'something went wrong this.js:11'
        const req = {
            body: ['test body'],
        }
        const res = new mockResp()

        errorFormat(err, req, res, null)

        assert.equal(res.statusCode, 500)
        assert.equal(res.body, JSON.stringify({
            error: true,
            message: 'Stack-Error',
            stack: [
                'not like this the-other-thing.js:111',
                'oh god that.js:118',
                'something went wrong this.js:11',
            ],
            _object: ['test body'],
        }))
    })
})

describe('boundary-joi-errors', () => {
    it('should return a 400 for Joi Validation errors', () => {
        const err = Joi.string().validate(3)
        const req = { body: 'body' }
        const res = new mockResp()
        errorFormat(err, req, res, null)
        assert.equal(res.statusCode, 400)
    })
    it('should return a 400 if any error is Joi', () => {
        const errors = [
            new Error('non-joi'),
            new Error('non-joi2'),
            Joi.string().validate(3),
            new Error('3rd non-joi'),
        ]
        const req = { body: 'body' }
        const res = new mockResp()
        errorFormat(errors, req, res, null)
        assert.equal(res.status(), 400)

    })
})
